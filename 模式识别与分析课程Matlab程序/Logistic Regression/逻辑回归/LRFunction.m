clc; clear all; close all;
% 封装好function 函数
%% 
data = [0 0 0; 0 1 0; 0 1.5 0; 0.5 0.5 0; 0.5 1 0; 1 0.95 0; 0.5 1.4 0; 1.5 0.51 0; 2 0 0; 
    1.9 0 0; 0 3 1; 0 2.1 1; 0.5 1.8 1; 0.8 1.5 1; 1 1.2 1; 1.5 2 1; 3 0 1; 3 1 1; 2 2 1; 
    3 4 1; 1.8 0.5 1];
figure
plot(data(1:10,1),data(1:10,2),'.','MarkerSize',20)
hold on
plot(data(11:21,1),data(11:21,2),'.','MarkerSize',20)

maxIndex = 2;
alpha = 0.1;
threshold = 0.00001;
maxTimes = 1000;
[theta,loss_list]=LogisticRegression(data, maxIndex, alpha, threshold, maxTimes)


test_data = rand(100,2)*2;
test_data(:,3)=1; %标签全为1
count = 0;
for i = 1 :length(test_data)
    pre_label = 0;
    temp = [1 test_data(i,1) test_data(i,2)];
    pre = 1/(1 + exp(-(theta' * temp')));
    if pre>0.5
        pre_label = 1;
    if pre_label==test_data(i,3)
        count=count+1;
    end
    end
end
acc = count/length(test_data);
fprintf("The test accuracy is  %f", acc);






%%     训练集回归,测试集测试仅通过假设函数给出预测结果即可
function [theta,loss_list]=LogisticRegression(data, maxIndex, alpha, threshold, maxTimes)
    %测试逻辑回归
    loss_list=[]
    dataSize = size(data);
    dataLen = dataSize(1);
    paramLen = maxIndex + 1;
    theta = zeros(paramLen, 1);
    theta0 = theta;
     
    times = 0;
    cost0 = 0;
    cost1 = 1;
     
    while abs(cost1-cost0) > threshold && times < maxTimes
        times = times + 1;
        theta0 = theta;
        cost0 = LogisticRegressionCostFun(theta, data);
        for i = 1 : dataLen
            tmp = ((1 / (1 + exp(-theta0' * [1, data(i,1:maxIndex)]'))) - data(i, paramLen)) / dataLen; 
            theta(1) = theta(1) - alpha * tmp;
            for j = 2 : paramLen
               theta(j) = theta(j) - alpha * tmp * data(i, j - 1); 
            end
        end
        loss_list=[loss_list;cost0];
        fprintf("epoch %4d: loss = %f", times, cost0);
        fprintf('\n');
        cost1 = LogisticRegressionCostFun(theta, data);
    end
end

function cost=LogisticRegressionCostFun(theta, data)
    %逻辑回归的代价函数计算
    paramLen = length(theta);
    X = zeros(paramLen, 1);
    dataSize = size(data);
    dataLen = dataSize(1);
    cost = 0;
    for i = 1 : dataLen
        X(1) = 1;
       for k = 1 : paramLen - 1
          X(k + 1) = data(i, k); 
       end
       cost = cost + (-data(i, 3) * log(1/(1 + exp(-(theta' * X)))) - (1 - data(i, 3)) * log(1 - 1/(1 + exp(-(theta' * X)))));
    end
    cost = cost / dataLen;
end

% %% 方法二、利用matlab自带的函数glmfit()
% 
% function theta=logisticRegression()
% % logistic regression的参数theta,可以用matlab自带函数glmfit求出
% x = [0.0 0.1 0.7 1.0 1.1 1.3 1.4 1.7 2.1 2.2]';
% y = [0 0 1 0 0 0 1 1 1 1]'; 
% theta = glmfit(x, [y ones(10,1)], 'binomial', 'link', 'logit');
% end








    