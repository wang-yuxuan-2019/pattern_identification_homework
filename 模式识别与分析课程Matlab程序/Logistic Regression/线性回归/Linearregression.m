clc;clear;close all;

%% 方法1，利用方程系数矩阵的逆直接求解
X = 0.1:0.1:10;       %产生首项为0.1，末尾项为10，公差为0.1的数组
Y=X + randn(1,100);   %产生与变量x等长度的数据，其值是在每个x数组元素上加上一个满足标准正态分布的随机数
plot(X,Y,'.')
hold on 
%% % 调用数据 
load data %实例数据
X=X';
Y=Y';

figure 
plot(X,Y,'.')
title('原始数据点') 


[N,M] = size(X);

S_X2 = sum(X.*X);
S_X   = sum(X);
S_XY = sum(X.*Y);
S_Y = sum(Y);

A1 = [S_X2,S_X;
         S_X ,  N];
B1 = [S_XY;
          S_Y];
coeff = inv(A1)*B1;

x_min = min(X);
x_max = max(X);
y_min = coeff(1)*x_min+coeff(2);
y_max = coeff(1)*x_max+coeff(2);


figure 
plot(X,Y,'.')

hold on
plot([x_min x_max],[y_min y_max], 'r-')
title('采用解析法求解') 


%% 方法2 利用迭代的方法求解

n_epoch = 3000;          %epoch size
a=1;
b=1;             %initial parameters
epsilon = 0.001;         %learning rate

for i =1:n_epoch
    for j =1:N
        a = a + epsilon*2*(Y(j) - a*X(j) - b)*X(j);
        b = b + epsilon*2*(Y(j) - a*X(j) - b);
    end

    L = 0;
    for j=1:N
        L = L + (Y(j)-a*X(j)-b)^2;
    end
    fprintf("epoch %4d: loss = %f, a = %f, b = %f", i, L, a, b);
    fprintf('\n');  %换行
    %print("epoch %4d: loss = %f, a = %f, b = %f" % (i, L, a, b))
end
x_min = min(X);
x_max = max(X);
y_min = a * x_min + b;
y_max = a * x_max + b;
figure 
plot(X,Y,'k.')
hold on 
plot([x_min x_max],[y_min y_max])


%% 方法二
clc;clear all; close all
load data %实例数据
X=X';
Y=Y';
[N,M] = size(X);

n_epoch = 3000;          %epoch size
a=1;
b=1;             %initial parameters
epsilon = 0.001;         %learning rate
threshold = 0.01;
times = 0;
cost0 = 0;
cost1 = 1;

while abs(cost1-cost0) > threshold && times < n_epoch

    times =times+1;
    
    L = 0;
    for j=1:N
        L = L + (Y(j)-a*X(j)-b)^2;
    end
    cost0 = L;
      
    
    for j =1:N
        a = a + epsilon*2*(Y(j) - a*X(j) - b)*X(j);
        b = b + epsilon*2*(Y(j) - a*X(j) - b);
    end
    
    
    L = 0;
    for j=1:N
        L = L + (Y(j)-a*X(j)-b)^2;
    end
    cost1 = L;  
    
    fprintf("epoch %4d: loss = %f, a = %f, b = %f", times, L, a, b);
    fprintf('\n');  %换行
    %print("epoch %4d: loss = %f, a = %f, b = %f" % (i, L, a, b))
end


x_min = min(X);
x_max = max(X);
y_min = a * x_min + b;
y_max = a * x_max + b;
figure 
plot(X,Y,'k.')
hold on 
plot([x_min x_max],[y_min y_max])












