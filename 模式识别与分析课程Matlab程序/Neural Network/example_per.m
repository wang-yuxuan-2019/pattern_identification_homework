clc
clear
%% 感知机伪代码
% 输入：T={(x1,y1),(x2,y2)...(xN,yN)}（其中xi∈X=Rn，yi∈Y={-1, +1}，i=1,2...N，学习速率为η）
% 输出：w, b;感知机模型f(x)=sign(w·x+b)
% (1) 初始化w0,b0
% (2) 在训练数据集中选取（xi, yi）
% (3) 如果yi(w * xi+b)≤0
%            w = w + ηyixi
%            b = b + ηyi
% (4) 如果所有的样本都正确分类，或者迭代次数超过设定值，则终止
% (5) 否则，跳转至（2）

%% 数据及参数
train_data = [1, 3,  1; 2, 5,  1; 3, 8,  1; 2, 6,  1; 
                       3, 1, -1;4, 1, -1;6, 2, -1;7, 3, -1];
eta=0.5;
n_iter=100;
weight = [0, 0]; %权重
bias = 0;  % 偏置量
learning_rate = eta;  % 学习速率
train_num = n_iter; % 迭代次数
%% 训练模型
for i =1:train_num
    fprintf("epoch is : %d   ",i)
    temp = train_data(randperm(8, 1),:);%随机一组数
    x1 =  temp(1,1);
    x2 =  temp(1,2);
    y=  temp(1,3);
    out = weight(1) * x1 + weight(2) * x2 + bias;
    if out>0
        predict = 1;
    else
        predict = -1;
    end
    fprintf('train data: x: (%2d, %2d) y: %2d  ==> predict: %2d' ,x1, x2, y, predict);
     fprintf("\n")
    if y * predict <= 0  % 判断误分类点
        weight(1) = weight(1) + learning_rate * y * x1;  % 更新权重
        weight(2) = weight(2) + learning_rate * y * x2;
        bias     = bias    + learning_rate * y;       % 更新偏置量
%         fprintf("update weight : (%2d, %2d) bias: %2d", weight(1), weight(2), bias)
%         fprintf("\n")
    end
end
%% 测试模型
y_pred = [];
for i = 1:length(train_data)
        temp = train_data(randperm(8, 1),:);%随机一组数
        x1 =  temp(1,1);
        x2 =  temp(1,2);
        y=  temp(1,3);
        out = weight(1)*x1 + weight(2)*x2 + bias;
        if out>0
            predict = 1;
        else
            predict = -1;
        end
        y_pred = [y_pred predict];
end
train_data(:,3)'
y_pred
