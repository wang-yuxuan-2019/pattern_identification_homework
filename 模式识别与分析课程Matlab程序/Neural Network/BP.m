clc
clear
%% 载入数据
load data.mat
figure
for i = 1:length(X)
    if y(i)==0
        plot(X(i,1),X(i,2),'ro')
        hold on
    else
        plot(X(i,1),X(i,2),'b*')
        hold on
    end
end

%% 参数设置
epsilon = 0.01;               % learning rate
n_epoch = 1000;             % iterative number
[m,n_input_dim] = size(X);      % input size
n_output_dim = 2  ;            % output node size
n_hide_dim = 4 ;            % hidden node size

W1 = rand(n_input_dim,n_hide_dim);  %输入到隐藏层权重
b1 = zeros(1,n_hide_dim);
W2 = rand(n_hide_dim,n_output_dim);  %隐藏到输出
b2 = zeros(1,n_output_dim);
y = y+1;
%% 
for epoch =1:n_epoch
    % 前向传播
    z1 = 1.0./(1+exp(-(X*W1 + b1)));
    z2 = 1.0./(1+exp(-(z1*W2 + b2)));
    % print loss, accuracy
    y_pred = [];
    for i = 1:length(z2)
        [m,y_pred_i]=max(z2(i,:));
        y_pred  = [y_pred;y_pred_i];
    end
    L = sum((y_pred - double(y')).^2);
    acc = sum(y'==y_pred)/length(y_pred);

    fprintf("epoch %d L = %f, acc = %f\n" , epoch, L, acc)

    % calc weights update
    d2 = z2.*(1-z2).*(t - z2);
    d1 = z1.*(1-z1).*(d2*W2');

    % update weights
    W2 = W2 + epsilon * (z1'*d2);
    b2 = b2 + epsilon * sum(d2,1);
    W1 = W1 + epsilon * X'*d1;
    b1 = b1 + epsilon * sum(d1,1);
end



