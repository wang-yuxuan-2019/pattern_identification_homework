%һ���ڶ�ͼ��
%% Introduce the the title , lable and legned of figures

t=-2*pi:0.01:2*pi
%�ֳ���������
subplot(3,2,1)
plot(t,sin(t))
%����ͼ����ʽ����ע
xlabel('t��')
ylabel('y��')
title("���Һ���")
legend('sin(t)')
%% How to use the hold on function
subplot(3,2,2)
plot(t,cos(t))
xlabel('t��')
ylabel('y��')
title("���Һ���")
hold on
plot(t,cos(2*t))
legend('cos(t)','cos2*(t)')
hold off

%% How to use axis  and color 

subplot(3,2,3)
plot(t,tan(t),'-r')
axis([-pi pi -100 100])
% Note that : Single defination of the x and y axis     xlim([a,b])  or ylim ([a,b])

xlabel('t��')
ylabel('y��')
title("���к���")
legend('tan(t)')


%% How to use different lines
subplot(3,2,4)
plot(t,cot(t),'--b')
axis([-pi pi -100 100])
xlabel('t��')
ylabel('y��')
title("���к���")
legend('cot(t)')


subplot(3,2,5)
plot(t,atan(t),':k')
xlabel('t��')
ylabel('y��')
title("�����к���")
legend('atan(t)')


subplot(3,2,6)
plot(t,acot(t),'-.g')
xlabel('t��')
ylabel('y��')
title("�����к���")
legend('acot(t)')


