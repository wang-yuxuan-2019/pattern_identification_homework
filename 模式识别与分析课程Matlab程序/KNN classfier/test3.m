%三维绘图
subplot(2,2,1)
x=0:pi/50:10*pi;
y=sin(x);
z=cos(x);
plot3(x,y,z);

%高斯分布函数网格图
subplot(2,2,2)
[x,y]=meshgrid(-3:0.5:3)
z=peaks(x,y);
meshz(x,y,z)
title("高斯分布函数")

%绘制三维饼图
subplot(2,2,3)
A=[1,2,3;4,5,6;7,8,9];
ex=[0,4,0;4,0,0;0,8,0];
pie3(A,ex)
title("三维饼图")

%绘制一个球面
subplot(2,2,4)
[x,y,z]=sphere(40);
surf(x,y,z)
title("球面")