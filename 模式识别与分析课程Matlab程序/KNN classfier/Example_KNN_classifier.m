%%算法描述
%1、初始化训练集和类别；
%2、计算测试集样本与训练集样本的欧氏距离；
%3、根据欧氏距离大小对训练集样本进行升序排序；
%4、选取欧式距离最小的前K个训练样本，统计其在各类别中的频率；
%5、返回频率最大的类别，即测试集样本属于该类别。
close all;clc;clear all;

%% 生成训练集
trainData1=[0 0;0.1 0.3;0.2 0.1;0.2 0.2];%第一类训练数据
trainData2=[1 0;1.1 0.3;1.2 0.1;1.2 0.2];%第二类训练数据
trainData3=[0 1;0.1 1.3;0.2 1.1;0.2 1.2];%第三类训练数据
trainData(:,:,1)=trainData1;%设置第一类训练数据
trainData(:,:,2)=trainData2;%设置第二类训练数据
trainData(:,:,3)=trainData3;%设置第三类训练数据
trainDim=size(trainData);%获取训练集的维数

%%   size的用法
% [a b] =  size (A); y = size (A,1);  y = size (A,2); 
%  数据其他存储方式  
% trainData = cell(1,3);
% trainData{1}=trainData1;%设置第一类训练数据
 %trainData{2}=trainData2;%设置第二类训练数据
 %trainData{3}=trainData3;%设置第三类训练数据

%   trainData{1}(1,:)  具体找出某一行
%%
figure(1);
hold on;
for i=1:4
    plot(trainData1(i,1),trainData1(i,2),'*');
    plot(trainData2(i,1),trainData2(i,2),'o');
    plot(trainData3(i,1),trainData3(i,2),'>');
end
% plot(testData(1),testData(2),'x');
text(0.1,0.1,'第一类');
text(1.1,0.1,'第二类');
text(0.1,1,'第三类');
title('三个训练数据集')

%% 生成测试样本
testData=rand(1,2);%Testing dat 

K=7;

testData_rep=repmat(testData,4,1)

% remat 命令讲解  ==复制或者平铺


%% 测试样本
% 计算测试样本的欧式距离，并统计出来
diff1=(trainData(:,:,1)-testData_rep).^2
diff2=(trainData(:,:,2)-testData_rep).^2
diff3=(trainData(:,:,3)-testData_rep).^2
%设置三个一维数组存放欧式距离
distance1=(diff1(:,1)+diff1(:,2)).^0.5
distance2=(diff2(:,1)+diff2(:,2)).^0.5
distance3=(diff3(:,1)+diff3(:,2)).^0.5

%% 将三个一维数组合成一个二维矩阵
temp=[distance1 distance2 distance3]
%将这个二维矩阵转换为一维数组
distance=reshape(temp,1,3*4)
%对距离进行排序
distance_sort=sort(distance,2)
% Sort 函数使用
% A = rand(4,3)
%  sort 函数: sort(A,'descend')实现从大到小排序；  
%             sort(A,'ascend')实现从小到大排序；

%              sort (A, 1) 按行重新排列原来的矩阵，从小到大
%              sort (A, 2) 按列重新排列原来的矩阵，从小到大
% [a b] = sort(distance,2)

%% 用循环寻找最小的k个距离里面那个类里出现的频率最高，并返回该类
num1=0; %初始化第一类出现的次数
num2=0; %初始化第二类出现的次数
num3=0; %初始化第三类出现的次数
sum=0; %初始化sum1,sum2,sum3的和
for i=1:K
    for j=1:4
        if distance1(j)==distance_sort(i)
            num1=num1+1;
        end
        if distance2(j)==distance_sort(i)
            num2=num2+1;
        end
        if distance3(j)==distance_sort(i)
            num3=num3+1;
        end
    end
    sum=num1+num2+num3;
    if sum>=K
        break;
    end
end

class=[num1 num2 num3]
classname=find(class(1,:)==max(class))
fprintf('测试点（%f %f）属于第%d类',testData(1),testData(2),classname);


%  fprintf 按指定的格式将变量的值输出到屏幕或指定文件
%d 整数
%e 实数：科学计算法形式
%f 实数：小数形式
%g 由系统自动选取上述两种格式之一


%%使用绘图将训练集点和测试集点绘画出来
figure(2);
hold on;
for i=1:4
    plot(trainData1(i,1),trainData1(i,2),'*');
    plot(trainData2(i,1),trainData2(i,2),'o');
    plot(trainData3(i,1),trainData3(i,2),'>');
end
plot(testData(1),testData(2),'x');
text(0.1,0.1,'第一类');
text(1.1,0.1,'第二类');
text(0.1,1,'第三类');



