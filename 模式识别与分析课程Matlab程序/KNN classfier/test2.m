%水平条形图
subplot(2,2,1)
y=[3 2 -2 2 1;-1 2 3 7 1;7 2 -3 5 2];
bar(y)
title('水平条形图')

%二维饼图
subplot(2,2,2)
x=[1,2,3;4,5,6;7,8,9]
explode=[0 1 0 1 0 1 0 1 0]
pie(x,explode)
title('二维饼图')

%矢量图
subplot(2,2,3)
x=0:0.1*pi:2*pi;
y=sin(x).*x
feather(x,y)
title("矢量图")

%极坐标图
subplot(2,2,4)
theta=[0:0.01:2*pi]
polar(theta,2*(1-cos(theta)),'-k')
polar(theta,2*(1-cos(theta)),'-r')
title('心形图')
