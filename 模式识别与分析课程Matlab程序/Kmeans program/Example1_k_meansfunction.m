clc; close all; clear all;

deg1=(2*rand(300,1)-1)*2*pi;
r1=rand(300,1);
deg2=(2*rand(30,1)-1)*2*pi;
r2=0.1*rand(30,1);
deg3=(2*rand(300,1)-1)*2*pi;
r3=rand(300,1);
X1=[r1.*cos(deg1) r1.*sin(deg1)];
X2=[r2.*cos(deg2)+5 r2.*sin(deg2)+5];
X3=[r3.*cos(deg3)+1 r3.*sin(deg3)+1];
X=[X1;X3];

%X=[X1;X2;X3];
 
 
%[idx,ctrs,D,sumD]=kmeans_mean(X,2);
[idx,ctrs]=kmeans(X,2);
 
figure
plot(X(idx==1,1),X(idx==1,2),'r.','MarkerSize',12)
hold on
plot(X(idx==2,1),X(idx==2,2),'b.','MarkerSize',12)
plot(ctrs(:,1),ctrs(:,2),'kx',...
     'MarkerSize',12,'LineWidth',2)
 plot(ctrs(:,1),ctrs(:,2),'ko',...
      'MarkerSize',12,'LineWidth',2)
legend('Cluster 1','Cluster 2','Centroids',...
       'Location','NW')
