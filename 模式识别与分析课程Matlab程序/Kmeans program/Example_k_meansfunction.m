%kmeans2norm K-means clustering.
% X:n*p的数据矩阵
% k:将X划分为几类
% startM:k*p的矩阵，初始类中心
% idx:n*1的向量，存储每个点的聚类编号
% C:k*p的矩阵，存储k个聚类的中心位置
% sumD:1*k的和向量，类间所有点与该类中心的距离之和
% D:n*k的矩阵，每一点与所有中心的距离
%sumK:1*k的和向量,记录第k个类中点的个数

clc; clear all; close all;

X = [randn(100,2)+ones(100,2);...
     randn(100,2)-ones(100,2)];  %上半加一下半减一，区分开来
k = 2; 


figure
plot(X(:,1),X(:,2),'r.','MarkerSize',12);

figure
plot(X(1:100,1),X(1:100,2),'r.','MarkerSize',12);
hold on
plot(X(101:200,1),X(101:200,2),'ks','MarkerSize',12);



[n,p] = size(X);         %  n 为数据的长度  p 为数据的列数
% idx=zeros(n,1);
Center=zeros(k,p);
startM=zeros(k,p); %初始聚类中心  
D=zeros(n,k);
 
%%                    -----------随机生成初始聚类中心---------------
%% ================Method 1============randperm(n,k)
% 演示  randperm(100,3)
k=2;

bi = randperm(n,k)
for i = 1:k
    startM (i,:) = X (bi (i),:)   
end

figure
plot(X(:,1),X(:,2),'r.','MarkerSize',12);
hold on
plot(startM(:,1),startM(:,2),'kx',...
     'MarkerSize',12,'LineWidth',2)
 plot(startM(:,1),startM(:,2),'ko',...
      'MarkerSize',12,'LineWidth',2)
legend('Cluster 1','Cluster 2','Centroids',...
       'Location','NW')


% or  讨论一下可不可以这样   
for i = 1:k
    bi = randperm(n,1)
    startM (i,:) = X (bi,:)   
end

% No     bi = randperm(n,1)  可能是重复的


% or  讨论一下可不可以这样  
%=====采用rand 函数体现此 MATLAB 函数 返回一个在区间 (0,1) 内均匀分布的随机数。 


for i=1:k
    bi=ceil(i*n/k*rand); % 随时生成 k 个  数据长度 N/k的值
    % fix保留整数部分；baifloor向下取整（去尾)；ceil向上取整（近一）。
    startM(i,:)=X(bi,:); % 随时选择2个聚类中心
end

% yes 

%%

figure
plot(X(:,1),X(:,2),'r.','MarkerSize',12);
hold on
plot(startM(:,1),startM(:,2),'kx',...
     'MarkerSize',12,'LineWidth',2)
 plot(startM(:,1),startM(:,2),'ko',...
      'MarkerSize',12,'LineWidth',2)
legend('Cluster 1','Cluster 2','Centroids',...
       'Location','NW')


   
   
   
   




%  while sum(abs(startM-C))>0 %聚类中心不变化作为循环的终止条件  采用   while  语句？Question：是否可以采用其他循环语句进行替换  

    sumD=zeros(1,k);  % sumD:1*k的和向量，类间所有点与该类中心的距离之和
    sumK=zeros(1,k);  %sumK:1*k的和向量,记录第k个类中点的个数
%%  ================计算每一点与所有中心的距离=================
% 目的是计算每一个点到聚类中心的距离，并记录各类的个数
% ============Method 1 ==================

Center=startM;

for i= 1:n
    D(i,1) =  sqrt((X(i,:)-Center(1,:))*(X(i,:)-Center(1,:))');
end



for i= 1:n
    D(i,2) =  sqrt((X(i,:)-Center(2,:))*(X(i,:)-Center(2,:))');
end




% ============思考如何用一个循环来解决这个问题====================

for i =1:n
    for j =1:k
        D(i,j) = sqrt((X(i,:)-Center(j,:))*(X(i,:)-Center(j,:))');
    end
end


% ============思考如何来判断这个点属于哪一类====================
% 采用排序函数即可完成上述工作  
[a b] = sort(D,2);

%  sort 函数: sort(A,'descend')实现从大到小排序；  
%             sort(A,'ascend')实现从小到大排序；

%              sort (A, 1) 按行重新排列原来的矩阵，从小到大
%              sort (A, 2) 按列重新排列原来的矩阵，从小到大
% [a b] = sort(distance,2)

% 第一种求和方式  遍历法
Num = 0; 
for i= 1:n
    if b(i,1)==1
       Num =Num+1;
    end
end

% 实现方案
 sumK(1) = 0;  sumK(2) = 0; 
for i= 1:n
    if b(i,1)==1
       sumK(1) =sumK(1)+1;
    end
    if b(i,1)==2
       sumK(2) =sumK(2)+1;
    end   
end

% 然而上述代码不具有通用性    
sumK=zeros(200,5);
for i= 1:n
    for j =1:k
        if b(i,1)==j
           sumK(j) =sumK(j)+1;
        end
    end
end


%==========================思考============================
Value = b(:,1); 
sumK(1)=sum(Value(Value==1))
sumK(2)=sum(Value(Value==2))/2
% =========================变换为循环的形式 使得具有通用性=================
Value = b(:,1); 
for i = 1:k
sumK(i)=sum(Value(Value==i))/i;
end

%==========================思考============================
length(find(b(:,1)==1))
length(find(b(:,1)==2))
% find find （X）返回非零元素的索引值
%[r,c,v] = find(X) 返回非零元素的行列和具体数值索引值
for i = 1:k   
sumK(i)=length(find(b(:,1)==i));
end

% ============思考如何来求这个点值====================
sumD=zeros(1,k);

for i= 1:n
    if b(i,1)==1
       sumK(1) =sumK(1)+1;   
       sumD(1,1)=sumD(1,1)+a(i,1);
    end
    if b(i,1)==2
       sumK(2) =sumK(2)+1;
       sumD(1,2) =sumD(1,2)+a(i,1);
    end   
end

% 然而上述代码不具有通用性    

for i= 1:n
    for j =1:k
        if b(i,1)==j
           sumK(j) =sumK(j)+1;
           sumD(j) =sumD(j)+a(i,1);
        end
    end
end

%==========================思考============================

Value = b(:,1); 
for i = 1:k

sumK(i)=sum(Value(Value==i))/i;
sumD(i) =sumD(i)+a(i,1);

end

% find find （X）返回非零元素的索引值
%[r,c,v] = find(X) 返回非零元素的行列和具体数值索引值

sumD=zeros(1,k);
for i = 1:k   
sumK(i)=length(find(b(:,1)==i));
[r,c,v] = find(b(:,1)==i);
sumD(i) =sum(a(r,1));
% sumD(i) =sum(a(find(b(:,1)==i),1));  %合并命令行
% clear r
end

figure
plot(X(b(:,1)==1,1),X(b(:,1)==1,2),'r.','MarkerSize',12)
hold on
plot(X(b(:,1)==2,1),X(b(:,1)==2,2),'b.','MarkerSize',12)

plot(startM(:,1),startM(:,2),'kx',...
     'MarkerSize',12,'LineWidth',2)
 plot(startM(:,1),startM(:,2),'ko',...
      'MarkerSize',12,'LineWidth',2)
legend('Cluster 1','Cluster 2','Centroids',...
       'Location','NW')
title('第一次的聚类效果')
    

pause
 %%  %---------计算新的聚类中心---------
startM(:)=0;
Cluster01=X(b(:,1)==1,:);
Cluster02=X(b(:,1)==2,:);
startM (1,:) = mean(Cluster01);
startM (2,:) = mean(Cluster02);
% mean(A,1) 返回列平均值  mean(A,2)返回行的平均值

hold on

plot(startM(:,1),startM(:,2),'gx',...
     'MarkerSize',12,'LineWidth',2)
 plot(startM(:,1),startM(:,2),'go',...
      'MarkerSize',12,'LineWidth',2)
legend('Cluster 1','Cluster 2','Centroids',...
       'Location','NW')
title('第一次的聚类效果')

pause
% =============思考=====具有通用性===============
startM(:)=0;
for i =1:k
    startM (i,:) = mean(X(b(:,1)==i,:));
end

%% %如何判定终止条件



while sum(abs(startM-Center))>0

   sumD=zeros(1,k);  % sumD:1*k的和向量，类间所有点与该类中心的距离之和
   sumK=zeros(1,k);  %sumK:1*k的和向量,记录第k个类中点的个数
    
 Center=startM;

    for i =1:n
        for j =1:k
            D(i,j) = sqrt((X(i,:)-Center(j,:))*(X(i,:)-Center(j,:))');
        end
    end   
 [a b] = sort(D,2);   
    
        for i = 1:k   
            sumK(i)=length(find(b(:,1)==i));
            [r,c,v] = find(b(:,1)==i);
            sumD(i) =sum(a(r,1));
            % sumD(i) =sum(a(find(b(:,1)==i),1));  %合并命令行
            clear r
        end
    
    startM(:)=0;
for i =1:k
    startM (i,:) = mean(X(b(:,1)==i,:));
end
        
end


hold on

plot(startM(:,1),startM(:,2),'cx',...
     'MarkerSize',12,'LineWidth',2)
 plot(startM(:,1),startM(:,2),'co',...
      'MarkerSize',12,'LineWidth',2)
legend('Cluster 1','Cluster 2','Centroids',...
       'Location','NW')
title('第一次的聚类效果')

pause


%% % 讲解如何变成一个function  

function [idex,startM]=k_means(X,k)

[n,p] = size(X);         %  n 为数据的长度  p 为数据的列数
% idx=zeros(n,1);
Center=zeros(k,p);
startM=zeros(k,p); %初始聚类中心
D=zeros(n,k);
k=2;
bi = randperm(n,k)
for i = 1:k
    startM (i,:) = X (bi (i),:)   
end

while sum(abs(startM-Center))>0

   sumD=zeros(1,k);  % sumD:1*k的和向量，类间所有点与该类中心的距离之和
   sumK=zeros(1,k);  %sumK:1*k的和向量,记录第k个类中点的个数
    
 Center=startM;

    for i =1:n
        for j =1:k
            D(i,j) = sqrt((X(i,:)-Center(j,:))*(X(i,:)-Center(j,:))');
        end
    end   
 [a b] = sort(D,2);   
    
        for i = 1:k   
            sumK(i)=length(find(b(:,1)==i));
            [r,c,v] = find(b(:,1)==i);
            sumD(i) =sum(a(r,1));
            % sumD(i) =sum(a(find(b(:,1)==i),1));  %合并命令行
            clear r
        end
    
    startM(:)=0;
for i =1:k
    startM (i,:) = mean(X(b(:,1)==i,:));
end
    idex = b(:,1);    
end
end


   
% 
%  figure
% plot(X(idx==1,1),X(idx==1,2),'r.','MarkerSize',12)
% hold on
% plot(X(idx==2,1),X(idx==2,2),'b.','MarkerSize',12)
% 
% 
% plot(startM(:,1),startM(:,2),'kx',...
%      'MarkerSize',12,'LineWidth',2)
%  plot(startM(:,1),startM(:,2),'ko',...
%       'MarkerSize',12,'LineWidth',2)
% legend('Cluster 1','Cluster 2','Centroids',...
%        'Location','NW')

 
 