function [idex,startM]=k_means(X,k)

[n,p] = size(X);         %  n 为数据的长度  p 为数据的列数
% idx=zeros(n,1);
Center=zeros(k,p);
startM=zeros(k,p); %初始聚类中心
D=zeros(n,k);
k=2;
bi = randperm(n,k)
for i = 1:k
    startM (i,:) = X (bi (i),:)   
end

while sum(abs(startM-Center))>0

   sumD=zeros(1,k);  % sumD:1*k的和向量，类间所有点与该类中心的距离之和
   sumK=zeros(1,k);  %sumK:1*k的和向量,记录第k个类中点的个数
    
 Center=startM;

    for i =1:n
        for j =1:k
            D(i,j) = sqrt((X(i,:)-Center(j,:))*(X(i,:)-Center(j,:))');
        end
    end   
 [a b] = sort(D,2);   
    
        for i = 1:k   
            sumK(i)=length(find(b(:,1)==i));
            [r,c,v] = find(b(:,1)==i);
            sumD(i) =sum(a(r,1));
            % sumD(i) =sum(a(find(b(:,1)==i),1));  %合并命令行
            clear r
        end
    
    startM(:)=0;
for i =1:k
    startM (i,:) = mean(X(b(:,1)==i,:));
end
    idex = b(:,1);    
end
end