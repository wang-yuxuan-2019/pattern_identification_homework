# 模式识别 - 作业提交说明

由于这门课程需要大量的编程、练习才能学好，因此需要大家积极把作业做好，通过作业、练习来牵引学习、提高解决问题的能力、自学等能力。

关于如何提交作业，如何使用git，word, markdown等等，可以参考下面使用帮助。

# 具体的操作步骤：

1. [大家fork这个项目到自己的项目](https://gitee.com/pi-lab/learn_programming/blob/master/6_tools/git/HowToForkClone.md)
2. [然后git clone自己的项目到本地机器](https://gitee.com/pi-lab/learn_programming/blob/master/6_tools/git/HowToForkClone.md)
3. 在作业的目录里写入各自的代码、报告等。
4. 通过`git push ...`上传作业到自己的项目里
5. 由于本作业会更新因此需要更新到最新的版本，可以如下操作

```
git remote add upstream git@gitee.com:liyongbo_nwpu/pattern_identification_homework.git
git pull upstream master
```

1. 在本目录新建一个`name.txt`文件（UTF-8编码），写下自己的名字和学号，例如

```
李永波
2011010101
```

大家提交作业后，我会在大家的项目里写入批注、建议等等，从而构建良好的反馈机制，能够更有效的取得学习效果。





## 使用帮助

- Git
  - [Git快速入门 - Git初体验](https://my.oschina.net/dxqr/blog/134811)
  - [在win7系统下使用TortoiseGit(乌龟git)简单操作Git](https://my.oschina.net/longxuu/blog/141699)
  - [Git系统学习 - 廖雪峰的Git教程](https://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000)
  - [Git使用教程](https://gitee.com/liyongbo_nwpu/pattern_identification_homework/blob/master/Gitee%E6%95%99%E7%A8%8B/Git%E4%BD%BF%E7%94%A8%E6%95%99%E7%A8%8B.pdf)
- Markdown
  - [Markdown——入门指南](https://www.jianshu.com/p/1e402922ee32)

