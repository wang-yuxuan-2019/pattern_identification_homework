% -------------------第一题-----------------------------------------
sum=0;
for i=0:20
    sum=sum+factorial(i);
end
disp(sum);

%-------------第二题------------------
x1=-pi/2:10^(-6):pi/2;
y1=sqrt(cos(x1));
subplot(2,2,2);
plot(x1,y1)
X2=-2:0.1:2;
Y2=-4:0.1:4;
[x2,y2]=meshgrid(X2,Y2);
f=x2.^2/4+y2.^2/16;
subplot(2,2,3);
mesh(f)


% -----------第三题---------------
u=0:0.1:pi;
v=0:0.1:pi;
x=(1+cos(u)).*cos(v);
y=(1+cos(u)).*sin(v);
z=sin(u);
subplot(1,3,1)
plot3(x,y,z)%用plot3画
[u1,v1]=meshgrid(u,v);
x=(1+cos(u1)).*cos(v1);
y=(1+cos(u1)).*sin(v1);
z=sin(u1);
subplot(1,3,2);
mesh(x,y,z)
subplot(1,3,3);
c=x.*y;
surf(x,y,z,c,'FaceAlpha','0.5')
colorbar

%---------第四题---------------------
x1=-5:0.1:5;
y1=1/(2*pi)*exp(-(x1.^2)/2);
subplot(1,2,1);
plot(x1,y1)
t=-1:0.1:1;
x2=t.^2;
y2=5*t.^3;
subplot(1,2,2);
plot(x2,y2)


%---------------第五题----------------



