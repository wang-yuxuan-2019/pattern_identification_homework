clc;clear;close all
load('data.mat')
[N1 N2]=size (Data);
Features = zeros(N1,2);

for i =1:N1
    Features(i,1) = rms(Data(i,:));
    Features(i,2) = mean(Data(i,:));  
end

% mean  skewness   peak  std kurtosis
clear Data
Data = Features;



lables = [repmat(1,[125,1]);repmat(2,[125,1]);repmat(3,[125,1]);repmat(4,[125,1])];
%B=repmat(A,n),该数组在其行维度和列维度包含 A 的 n 个副本。A 为矩阵时，B 大小为 size(A)*n



%%  划分数据集
[TrainingData,TrainingLabel,TestData,TestLabel] = split_train_test(Data, lables, 4, 0.7);



%% use the knn to classify the fault
disp('KNN');
k = 7;
predict_label_L = knnclassification(TestData,TrainingData, TrainingLabel, k); 
%-------------3. Accuracy-------------%
right = find(predict_label_L == TestLabel);
Accuracy1 = numel(right) / length(TestLabel);

fprintf('轴承故障识别率为%6.2f\n',Accuracy1*100)
huafenleitu(4,length(TestLabel)/4,predict_label_L )

disp('LR');

predict_label_P=LR(TrainingData,TestData,TrainingLabel,TestLabel);
right = find(predict_label_P == TestLabel);
Accuracy1 = numel(right) / length(TestLabel);

fprintf('轴承故障识别率为%6.2f\n',Accuracy1*100)
huafenleitu(4,length(TestLabel)/4,predict_label_P )





function predict_label_P=LR(TrainingData,TestData,TrainingLabel,TestLabel)

lambda = [0.001,0.01,0.1,1,10,100];% 做多次循环求最大值
%     lambda = 0.001;
    Num = length(lambda);
    Accuracy = zeros(Num,1);

for i = 1:Num
    
    [model, llh] = logitMn(TrainingData',TrainingLabel',lambda(i));
    predict_label_L = logitMnPred(model,TestData');
    
    %-------------3. Accuracy-------------%
    right = find(predict_label_L == TestLabel');
    Accuracy(i,1) = numel(right) / length(TestLabel);
 %   fprintf('>>>>k = %d--------Test Accuracy:%f\n', lambda(i),Accuracy(i,1));
end
    LR_Accuracy=max(Accuracy);
    
   h = LR_Accuracy;
   predict_label_P = predict_label_L';
end   
   
   
   
   
   
   
   function [X_train, y_train,  X_test, y_test] = split_train_test(X, y, k, ratio)
%SPLIT_TRAIN_TEST 分割训练集和测试集
%  参数X是数据矩阵 y是对应类标签 k是类别个数 ratio是训练集的比例
%  返回训练集X_train和对应的类标签y_train 测试集X_test和对应的类标签y_test
m = size(X, 1);
y_labels = unique(y); % 去重，k应该等于length(y_labels) 
d = [1:m]';
X_train = [];
y_train= [];
for i = 1:k
    comm_i = find(y == y_labels(i));
    if isempty(comm_i) % 如果该类别在数据集中不存在
        continue;
    end
    size_comm_i = length(comm_i);
    rp = randperm(size_comm_i); % random permutation
    rp_ratio = rp(1:floor(size_comm_i * ratio));
    ind = comm_i(rp_ratio);
    X_train = [X_train; X(ind, :)];
    y_train = [y_train; y(ind, :)];
    d = setdiff(d, ind);
end
X_test = X(d, :);
y_test = y(d, :);
end

%%   ％ＫＮＮ分类器

function result = knnclassification(testsamplesX,samplesX, samplesY, Knn,type)

% Classify using the Nearest neighbor algorithm
% Inputs:
% 	samplesX	   - Train samples
%	samplesY	   - Train labels
%   testsamplesX   - Test  samples
%	Knn		       - Number of nearest neighbors 
%
% Outputs
%	result	- Predicted targets
if nargin < 5
    type = '2norm';
end

L			= length(samplesY);
Uc          = unique(samplesY);

if (L < Knn),
   error('You specified more neighbors than there are points.')
end

N                   = size(testsamplesX, 1);
result              = zeros(N,1); 
switch type
case '2norm'
    for i = 1:N,
        dist            = sum((samplesX - ones(L,1)*testsamplesX(i,:)).^2,2);
        [m, indices]    = sort(dist);  
        n               = hist(samplesY(indices(1:Knn)), Uc);
        [m, best]       = max(n);
        result(i)        = Uc(best);
    end
case '1norm'
    for i = 1:N,
        dist            = sum(abs(samplesX - ones(L,1)*testsamplesX(i,:)),2);
        [m, indices]    = sort(dist);   
        n               = hist(samplesY(indices(1:Knn)), Uc);
        [m, best]       = max(n);
        result(i)        = Uc(best);
    end
case 'match'
    for i = 1:N,
        dist            = sum(samplesX == ones(L,1)*testsamplesX(i,:),2);
        [m, indices]    = sort(dist);   
        n               = hist(samplesY(indices(1:Knn)), Uc);
        [m, best]       = max(n);
        result(i)        = Uc(best);
    end
otherwise
    error('Unknown measure function');
end
end



function huafenleitu(jilei,testnumb,predict_label_L )

%%%%%=============输入是列向量%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%要画几类的分类图 jilei，，，，目前只有10类以内，要加类在颜色里多加几个就行
%%%%%%%一类中有多少个样本个数testnumb
%%%%%%预测标签，为列向量predict_label_L
if jilei>10
    error('要画10类以上的，往颜色列表里多加几行颜色，并修改本句');
end


%%%%%自动写出TestLabel
for i=1:jilei
   for j=1:testnumb 
     TestLabel(j+testnumb*(i-1),1)=i;
   end
end

N = length(TestLabel);
N1 = max(TestLabel)+1;
%%%%%=========输出当前的测试精度
right = find(predict_label_L == TestLabel);
 fprintf('Right number: %d-----Test number:%d', numel(right),length(TestLabel));
 fprintf('\n');  %换行
 
 
%%%%%%%%%画图开始
figure
plot(1:N,TestLabel(1:end),'s','markeredgecolor',[.3,.3,.9],'markersize',10);%%%%预测标签
hold on

%%%%%%%%%%%%%%颜色列表%%%%%%%%%%%%%%%
% a=[ 0   0   1      %蓝                1
%  1   1   0        %  黄               2
% 1   0   1         %洋红               3
% 0   1   1        % 青蓝               4
% 1   0   0         %  红               5
% 0.67 0   1       %  紫色              6
% 0   1   0       %   绿                7
% 1 0.5 0         %   橘黄              8
% 0.5 0   0      %  深红                9
% 0.5 0.5 0.5  %  灰                    10
%    ] ;

a=repmat([1,0,0],10,1);



%%%%%%%%%画点
for i=1:jilei;
 plot((1+testnumb*(i-1):testnumb*i),predict_label_L(1+testnumb*(i-1):testnumb*i),'o','markerfacecolor',a(i,:),'markeredgecolor','k','markersize',5);%%%实际标签
hold on
end

%%%%%%%%%%画箭头，注意，箭头的坐标是归一化了的
ylim([0,N1])
xlim([0,N])

for i=1:N
    if predict_label_L(i)==TestLabel(i)
    else

   %quiver(i,TestLabel(i),0,predict_label_L(i)-TestLabel(i),'MaxHeadSize',0.2,'AutoScaleFactor',0.89,'AutoScale','off','Color_I','r');%%%%%%%%%%%%%箭头太小，但是有自动颜色，去掉'Color_I','r'这个
 
    x1(1)=i;
    x1(2)=i;
    y1(1)=TestLabel(i);
    y1(2)=predict_label_L(i);

    [x2,y2]=dsxy2figxy(gca,x1,y1);
%     annotation('arrow',x2,y2,'color',[1 0 0],'HeadStyle','plain'); 
    annotation('arrow',x2,y2,'color',[1 0 0],'HeadStyle','vback1');
   
    end
end




set(gca,'ytick',0:1:N1)
hold off
grid on

% xlabel('\fontsize{12}Sample number')
% ylabel('\fontsize{12}Sample labels')
% %title('\fontsize{12}Classification Results')
% legend('Real Class','Classification Output')

xlabel('\fontsize{12}样本数/\itN')
ylabel('\fontsize{12}样本标签')
%title('\fontsize{12}Classification Results')
legend('Real Class','Classification Output')

% set(gca,'Fontname','times new Roman');
end




function varargout = dsxy2figxy(varargin)
%%%%%转化坐标，只是为了画箭头
if length(varargin{1}) == 1 && ishandle(varargin{1}) ...
                            && strcmp(get(varargin{1},'type'),'axes')   
    hAx = varargin{1};
    varargin = varargin(2:end);
else
    hAx = gca;
end;
if length(varargin) == 1
    pos = varargin{1};
else
    [x,y] = deal(varargin{:});
end
axun = get(hAx,'Units');
set(hAx,'Units','normalized'); 
axpos = get(hAx,'Position');
axlim = axis(hAx);
axwidth = diff(axlim(1:2));
axheight = diff(axlim(3:4));
if exist('x','var')
    varargout{1} = (x - axlim(1)) * axpos(3) / axwidth + axpos(1);
    varargout{2} = (y - axlim(3)) * axpos(4) / axheight + axpos(2);
else
    pos(1) = (pos(1) - axlim(1)) / axwidth * axpos(3) + axpos(1);
    pos(2) = (pos(2) - axlim(3)) / axheight * axpos(4) + axpos(2);
    pos(3) = pos(3) * axpos(3) / axwidth;
    pos(4) = pos(4) * axpos(4 )/ axheight;
    varargout{1} = pos;
end
set(hAx,'Units',axun)
end
   
   