# 第二次作业

### 熟悉各个数据库使用

http://www.ics.uci.edu/~mlearn/MLRepository.html

UCI机器学习数据库

http://blog.csdn.net/v_july_v/article/details/6142146

数据挖掘领域十大经典算法

http://blog.csdn.net/v_july_v/article/details/7577684

### 完成

一、简述KNN 的算法流程

二、找一个案例或随机生成分类数组进行KNN分类，并给出识别率，画出分类图

注：算法基本思路整理成 word, markdown等，上传到发到Gitee