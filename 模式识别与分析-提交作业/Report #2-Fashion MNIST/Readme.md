# Report 2 - Fashion-MNIST图像识别

## 内容：
* 任务类型：十分类问题

* 背景介绍：Fashion-MNIST是一个图像数据集，由Zalando（一家德国的时尚科技公司）旗下的研究部门提供。其涵盖了来自10种类别的共7万个不同商品的正面图片。Fashion-MNIST数据集包含了10个类别的图像，分别是：t-shirt（T恤），trouser（牛仔裤），pullover（套衫），dress（裙子），coat（外套），sandal（凉鞋），shirt（衬衫），sneaker（运动鞋），bag（包），ankle boot（短靴）。

  	![image-2](Figures/2.png)。

* 数据介绍： 训练数据集每个类别含有6000个样本，测试数据集每个类别含有1000个样本，一共有10个类别，故而训练数据集共60000个样本，测试数据集共10000个样本。如下
	1. 60000张训练图像和对应Label；
	2. 10000张测试图像和对应Label；
	3. 10个类别；
	4. 每张图像28x28的分辨率；
	
	![image-1](Figures/1.png)

* **数据下载以及具体数据说明可参考[FASHION MNIST](https://github.com/zalandoresearch/fashion-mnist "fashion_mnist")**

* 关于如何使用matla进行图像数据的读取，可参考博客[Blog_mnist](https://blog.csdn.net/tracer9/article/details/51253604 "Blog_mnist")


* 评价方法：Accuracy 

  

## 要求：

1. 编写程序，初步完成分类
2. 分析结果的效果，综合考虑各种方法，改进方法
3. 撰写自己的报告













# Report - 报告包含内容

- 姓名
- 学号

## 任务简介

这里简述一下任务是什么；数据的格式，包含了什么数据；最终的目标是什么

## 解决途径

主要包括：

1. 问题的思考，整体的思路
2. 选用的方法，以及为何选用这些方法
3. 实现过程遇到的问题，以及如何解决的
4. 最终的结果，实验分析

要求：

1. 数据的可视化
2. 程序，以及各个部分的解释、说明
3. 结果的可视化，精度等的分析

## 总结

总结任务实现过程所取得的心得等。







